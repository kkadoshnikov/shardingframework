package net.thumbtack.kadoshnikov.sharding.framework.util;

/**
 * Created by kkadoshnikov on 8/25/15.
 */
@FunctionalInterface
public interface QueryClosureDecorator {

    <T> QueryClosure<T> decorate(QueryClosure<T> queryClosure);

}
