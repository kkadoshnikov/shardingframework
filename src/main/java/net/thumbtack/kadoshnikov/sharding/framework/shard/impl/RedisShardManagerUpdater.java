package net.thumbtack.kadoshnikov.sharding.framework.shard.impl;

import net.thumbtack.kadoshnikov.sharding.framework.shard.Shard;
import net.thumbtack.kadoshnikov.sharding.framework.shard.ShardManager;
import net.thumbtack.kadoshnikov.sharding.framework.util.Constants;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class RedisShardManagerUpdater {
    private final RedisShardManagerConfigurator redisShardManagerConfigurator;
    private final ShardManager manager;
    private JedisPool pool;
    private ScheduledExecutorService updateService;

    public RedisShardManagerUpdater(RedisShardManagerConfigurator redisShardManagerConfigurator, ShardManager manager) {
        this.manager = manager;
        this.redisShardManagerConfigurator = redisShardManagerConfigurator;
    }

    public void changePool(String confHost) {
        if (pool != null) {
            pool.close();
        }
        pool = new JedisPool(confHost);
    }

    public void changePeriod(long confPeriod) {
        if (updateService != null) {
            updateService.shutdown();
        }
        updater(confPeriod, TimeUnit.MILLISECONDS);
    }

    private void updater(long period, TimeUnit timeUnit) {
        updateService = Executors.newSingleThreadScheduledExecutor();
        updateService.scheduleAtFixedRate(this::update, 0, period, timeUnit);
    }

    private void update() {
        updateShards();
        manager.configure(getMapValues(redisShardManagerConfigurator.getMapName()));
    }

    private void updateShards() {
        Set<String> names = getSet(redisShardManagerConfigurator.getShardSetName());
        deleteRemovedShards(names);
        addNewShards(names);
    }

    private void deleteRemovedShards(Set<String> names) {
        Set<String> toDel = getShardsNames();
        toDel.removeAll(names);
        toDel.forEach(manager::removeShard);
    }

    private void addNewShards(Set<String> names) {
        Set<String> newNames = new HashSet<>(names);
        newNames.removeAll(getShardsNames());
        newNames.stream().forEach(this::addShard);
    }

    private void addShard(String shardName) {
        Map<String, String> fields = getMapValues(shardName);
        Shard shard = new ShardImpl(shardName, fields.get(Constants.URL_KEY), fields.get(Constants.USER_KEY),
                fields.get(Constants.PASS_KEY));
        manager.addShard(shard);
    }

    public Map<String, String> getMapValues(String key) {
        try (Jedis jedis = pool.getResource()) {
            return jedis.hgetAll(key);
        }
    }

    public Set<String> getSet(String key) {
        try (Jedis jedis = pool.getResource()) {
            return jedis.smembers(key);
        }
    }

    private Set<String> getShardsNames() {
        return manager.shards().stream().map(Shard::getName).collect(Collectors.toSet());
    }

}