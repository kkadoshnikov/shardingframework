package net.thumbtack.kadoshnikov.sharding.framework.shard;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Created by kkadoshnikov on 8/20/15.
 */
public interface ShardManager {

    void configure(Map<String, String> mapping);

    Set<Shard> shards();

    void addShard(Shard shard);

    Optional<Shard> getShardByName(String name);

    void removeShard(String shardName);

    void addId(String shardName, String id);

    void removeId(String id);

    Optional<Shard> getShard(String id);

    Optional<Shard> getBalanceShard();

}
