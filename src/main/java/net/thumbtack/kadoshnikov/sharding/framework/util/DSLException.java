package net.thumbtack.kadoshnikov.sharding.framework.util;

/**
 * Created by kkadoshnikov on 8/27/15.
 */
public class DSLException extends RuntimeException {

    public DSLException() {
        super("execute error");
    }

    public DSLException(String message) {
        super(message);
    }

}
