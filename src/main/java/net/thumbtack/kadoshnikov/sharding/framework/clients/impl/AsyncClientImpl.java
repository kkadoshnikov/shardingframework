package net.thumbtack.kadoshnikov.sharding.framework.clients.impl;

import net.thumbtack.kadoshnikov.sharding.framework.clients.AsyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.util.ExecuteUnknownResult;
import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;
import net.thumbtack.kadoshnikov.sharding.framework.clients.SyncClient;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * Created by kkadoshnikov on 8/19/15.
 */
public class AsyncClientImpl implements AsyncClient {

    private SyncClient client;

    public AsyncClientImpl(SyncClient syncClient) {
        client = syncClient;
    }

    @Override
    public <T> CompletableFuture<Optional<T>> selectSpec(String id, QueryClosure<T> queryClosure) {
        return CompletableFuture.supplyAsync(() -> client.selectSpec(id, queryClosure));
    }

    @Override
    public <T> CompletableFuture<Optional<T>> select(QueryClosure<T> queryClosure) {
        return CompletableFuture.supplyAsync(() -> client.select(queryClosure));
    }

    @Override
    public <T> CompletableFuture<List<T>> selectAll(QueryClosure<List<T>> queryClosure) {
        return CompletableFuture.supplyAsync(() -> client.selectAll(queryClosure));
    }

    @Override
    public CompletableFuture<Optional<Integer>> execute(String shardName, QueryClosure<Integer> queryClosure) {
        return CompletableFuture.supplyAsync(() -> client.execute(shardName, queryClosure));
    }

    @Override
    public CompletableFuture<Optional<Integer>> executeSpec(String id, QueryClosure<Integer> queryClosure) {
        return CompletableFuture.supplyAsync(() -> client.executeSpec(id, queryClosure));
    }

    @Override
    public CompletableFuture<Optional<ExecuteUnknownResult>> executeUnknownSpec(QueryClosure<Integer> queryClosure) {
        return CompletableFuture.supplyAsync(() -> client.executeUnknownSpec(queryClosure));
    }

    @Override
    public  CompletableFuture<String> balanceInsert(QueryClosure<Integer> queryClosure) {
        return CompletableFuture.supplyAsync(() -> client.balanceInsert(queryClosure));
    }

}
