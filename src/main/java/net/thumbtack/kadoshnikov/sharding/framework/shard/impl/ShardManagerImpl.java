package net.thumbtack.kadoshnikov.sharding.framework.shard.impl;

import net.thumbtack.kadoshnikov.sharding.framework.shard.Shard;
import net.thumbtack.kadoshnikov.sharding.framework.shard.ShardManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.locks.Lock;

/**
 * Created by kkadoshnikov on 8/20/15.
 */
public class ShardManagerImpl implements ShardManager {

    private Map<String, Shard> shards = new ConcurrentHashMap<>();
    private volatile NavigableMap<String, Integer> rowCounts = new ConcurrentSkipListMap<>();
    private volatile Map<String, String> shardMapping = new HashMap<>();

    @Override
    public void configure(Map<String, String> mapping) {
        shardMapping = mapping;
        Map<String, Integer> newCounts = new HashMap<>();
        shardMapping.forEach((id, shard) -> newCounts.put(shard, newCounts.containsKey(shard) ?
                newCounts.get(shard) + 1 : 1));
        NavigableMap<String, Integer> newSkipListCount = new ConcurrentSkipListMap<>();
        newCounts.forEach(newSkipListCount::put);
        rowCounts = newSkipListCount;
    }

    @Override
    public Set<Shard> shards() {
        return new HashSet<>(shards.values());
    }

    @Override
    public void addShard(Shard shard) {
        shards.put(shard.getName(), shard);
    }

    @Override
    public Optional<Shard> getShardByName(String name) {
        return Optional.ofNullable(shards.get(name));
    }

    @Override
    public void removeShard(String shardName) {
        shards.remove(shardName);
    }

    @Override
    public void addId(String shardName, String id) {
        shardMapping.putIfAbsent(id, shardName);
    }

    @Override
    public void removeId(String id) {
        shardMapping.remove(id);
    }

    @Override
    public Optional<Shard> getShard(String id) {
        String shardName = shardMapping.get(id);
        if (shardName == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(shards.get(shardName));
    }

    @Override
    public Optional<Shard> getBalanceShard() {
        return Optional.ofNullable(shards.get(rowCounts.firstKey()));
    }

}
