package net.thumbtack.kadoshnikov.sharding.framework.dsl.queries;

import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;

import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kkadoshnikov on 8/28/15.
 */
public class Update extends Query<Update> {

    private Map<String, String> updateValues = new HashMap<>();
    private String predicate;
    private String table;
    private String idField;
    private String id = "";

    private Update() {

    }

    private Update(String id) {
        this.id = id;
    }

    public static Update update() {
        return new Update();
    }

    public static Update update(String id) {
        return new Update(id);
    }

    public Update setIdField(String idField) {
        this.idField = idField;
        return this;
    }

    public Update set(String field, String value) {
        updateValues.put(field, value);
        return this;
    }

    public Update where(String predicate) {
        this.predicate = predicate;
        return this;
    }

    public Update from(String table) {
        this.table = table;
        return this;
    }

    public QueryClosure<Integer> execute() {
        return conn -> {
            try (Statement st = conn.createStatement()) {
                return st.executeUpdate(buildQuery().toString());
            }
        };
    }

    protected StringBuilder buildQuery() {
        StringBuilder query = new StringBuilder("UPDATE `");
        query.append(table).append("`");
        query.append("SET");
        updateValues.forEach((field, value) -> query.append("`").append(field).append("`='").append(value).append("', "));
        query.deleteCharAt(query.length() - 1);
        query.deleteCharAt(query.length() - 1);
        query.append(" WHERE ");
        if (id.isEmpty()) {
            query.append(predicate);
        } else {
            query.append(idField).append(" = ").append(id);
        }
        query.append(";");
        return query;
    }

}
