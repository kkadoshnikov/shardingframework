package net.thumbtack.kadoshnikov.sharding.framework.dsl.queries;

import net.thumbtack.kadoshnikov.sharding.framework.util.DSLException;
import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;

import java.sql.Statement;

/**
 * Created by kkadoshnikov on 8/27/15.
 */
public class Insert extends Query<Insert> {

    private String[] fields;
    private String[] values;
    private String table;

    private Insert(String... fields) {
        this.fields = fields;
    }

    public static Insert insert(String... fields) {
        return new Insert(fields);
    }

    public Insert into(String table) {
        this.table = table;
        return this;
    }

    public Insert values(String... values) {
        this.values = values;
        return this;
    }

    protected StringBuilder buildQuery() {
        StringBuilder query = new StringBuilder("INSERT INTO `");
        query.append(table).append("`");
        query = appendArgs(query, fields, '`');
        query.append(" VALUES");
        query = appendArgs(query, values, '\'');
        query.append(";");
        return query;
    }

    public String getId(String idField) {
        int index = -1;
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].equals(idField)) {
                index = i;
            }
        }
        if (index == -1) {
            throw new DSLException("query error");
        }
        return values[index];
    }

}
