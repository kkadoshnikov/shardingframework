package net.thumbtack.kadoshnikov.sharding.framework.dsl.clients;

import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.DslQuery;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * Created by kkadoshnikov on 8/31/15.
 */
public interface DslAsyncClient {

    <T> CompletableFuture<Optional<T>> execute(DslQuery query);

}
