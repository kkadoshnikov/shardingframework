package net.thumbtack.kadoshnikov.sharding.framework.util;

import java.sql.Connection;

/**
 * Created by kkadoshnikov on 8/18/15.
 */
@FunctionalInterface
public interface QueryClosure<T> {
    T call(Connection conn) throws Exception;
}
