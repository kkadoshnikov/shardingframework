package net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.impl;

import net.thumbtack.kadoshnikov.sharding.framework.clients.impl.RedisClientBuilder;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.DslAsyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.DslHandler;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.DslSyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.shard.ShardManager;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kkadoshnikov on 8/27/15.
 */
public class DslClientBuilder {

    private ShardManager manager;
    private String idField;
    private Map<Class, DslHandler> handlers = new HashMap<>();

    private RedisClientBuilder redisClientBuilder = new RedisClientBuilder();

    public DslClientBuilder withDriver(String driver) {
        redisClientBuilder.withDriver(driver);
        return this;
    }

    public DslClientBuilder withShardManager(ShardManager manager) {
        this.manager = manager;
        redisClientBuilder.withShardManager(manager);
        return this;
    }

    public DslClientBuilder withTransaction() {
        redisClientBuilder.withTransaction();
        return this;
    }

    public DslClientBuilder withConfigFile(File file) {
        redisClientBuilder.withConfigFile(file);
        return this;
    }

    public DslClientBuilder withIdField(String idField) {
        this.idField = idField;
        return this;
    }

    public DslClientBuilder withHandler(Class queryClass, DslHandler handler) {
        handlers.put(queryClass, handler);
        return this;
    }

    public DslSyncClient sync() {
        return new DslSyncClientImpl(idField, redisClientBuilder.sync(), manager, handlers);
    }

    public DslAsyncClient async() {
        return new DslAsyncClientImpl(new DslSyncClientImpl(idField, redisClientBuilder.sync(), manager, handlers));
    }

}
