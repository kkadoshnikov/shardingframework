package net.thumbtack.kadoshnikov.sharding.framework.dsl.queries;

import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;

import java.sql.Statement;

/**
 * Created by kkadoshnikov on 8/27/15.
 */
public abstract class Query<T extends Query> implements DslQuery{

    private String shard = "";

    public String getShard() {
        return shard;
    }

    @SuppressWarnings("unchecked")
    public QueryClosure<Integer> execute() {
        return conn -> {
            try (Statement st = conn.createStatement()) {
                return st.executeUpdate(buildQuery().toString());
            }
        };
    }

    @SuppressWarnings("unchecked")
    public T shard(String shard) {
        this.shard = shard;
        return (T) this;
    }

    public static StringBuilder appendArgs(StringBuilder query, String[] strings, char c) {
        query.append(" (");
        for (String field:strings) {
            query.append(c).append(field).append(c).append(", ");
        }
        query.deleteCharAt(query.length() - 1);
        query.deleteCharAt(query.length() - 1);
        query.append(")");
        return query;
    }

    abstract protected StringBuilder buildQuery();

}
