package net.thumbtack.kadoshnikov.sharding.framework.clients.impl;

import net.thumbtack.kadoshnikov.sharding.framework.clients.AsyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.clients.ClientBuilder;
import net.thumbtack.kadoshnikov.sharding.framework.clients.SyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.shard.impl.RedisShardManagerConfigurator;

import java.io.File;

/**
 * Created by kkadoshnikov on 8/25/15.
 */
public class RedisClientBuilder extends ClientBuilder<RedisClientBuilder> {

    private File conf;

    public RedisClientBuilder withConfigFile(File file) {
        conf = file;
        return this;
    }

    @Override
    public SyncClient sync() {
        new RedisShardManagerConfigurator(manager, conf);
        return new SyncClientImpl(manager, driver, decorator);
    }

    @Override
    public AsyncClient async() {
        new RedisShardManagerConfigurator(manager, conf);
        return new AsyncClientImpl(new SyncClientImpl(manager, driver, decorator));
    }

}
