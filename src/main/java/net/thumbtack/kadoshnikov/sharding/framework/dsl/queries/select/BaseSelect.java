package net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select;

import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.DslQuery;
import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 * Created by kkadoshnikov on 8/27/15.
 */
public abstract class BaseSelect<T extends BaseSelect> implements DslQuery {

    protected String table;

    @SuppressWarnings("unchecked")
    public T from(String table) {
        this.table = table;
        return (T) this;
    }

    protected String[] getFields(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        String[] fields = new String[metaData.getColumnCount()];
        for (int i = 0; i < fields.length; i++) {
            fields[i] = metaData.getColumnName(i + 1);
        }
        return fields;
    }

    protected Map<String, Object> getResult(ResultSet rs, String[] fields) throws SQLException {
        Object[] values = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            values[i] = rs.getObject(i + 1);
        }
        Map<String, Object> result = new HashMap<>();
        for (int i = 0; i < fields.length; i++) {
            result.put(fields[i], values[i]);
        }
        return result;
    }

    abstract protected StringBuilder buildQuery();

}
