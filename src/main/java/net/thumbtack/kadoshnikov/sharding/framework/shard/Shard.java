package net.thumbtack.kadoshnikov.sharding.framework.shard;

import java.sql.Connection;

/**
 * Created by kkadoshnikov on 8/21/15.
 */
public interface Shard {

    Connection getConnection() throws Exception;

    String getName();

}
