package net.thumbtack.kadoshnikov.sharding.framework.dsl.clients;

import net.thumbtack.kadoshnikov.sharding.framework.clients.SyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.DslQuery;
import net.thumbtack.kadoshnikov.sharding.framework.shard.ShardManager;

import java.util.Optional;

/**
 * Created by kkadoshnikov on 8/31/15.
 */
@FunctionalInterface
public interface DslHandler<T> {

    Optional<T> handle(DslQuery dslQuery, ShardManager shardManager, SyncClient syncClient);

}
