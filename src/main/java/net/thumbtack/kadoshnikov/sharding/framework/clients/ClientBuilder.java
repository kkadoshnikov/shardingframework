package net.thumbtack.kadoshnikov.sharding.framework.clients;

import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;
import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosureDecorator;
import net.thumbtack.kadoshnikov.sharding.framework.util.ShardException;
import net.thumbtack.kadoshnikov.sharding.framework.shard.ShardManager;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by kkadoshnikov on 8/25/15.
 */
abstract public class ClientBuilder <T extends ClientBuilder> {

    protected String driver;
    protected ShardManager manager;
    protected QueryClosureDecorator decorator = this::identity;

    @SuppressWarnings("unchecked")
    public T withDriver(String driver) {
        this.driver = driver;
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T withShardManager(ShardManager manager) {
        this.manager = manager;
        return (T) this;
    }


    @SuppressWarnings("unchecked")
    public T withTransaction() {
        decorator = this::transactional;
        return (T) this;
    }

    abstract public SyncClient sync();

    abstract public AsyncClient async();

    private <R> QueryClosure<R> identity(QueryClosure<R> queryClosure) {
        return queryClosure;
    }

    private <R> QueryClosure<R> transactional(QueryClosure<R> queryClosure) {
        return conn -> {
            try {
                return execute(queryClosure, conn);
            } catch (Exception e) {
                return rollback(conn, e);
            }
        };
    }

    private <R> R execute(QueryClosure<R> queryClosure, Connection conn) throws Exception {
        conn.setAutoCommit(false);
        R r = queryClosure.call(conn);
        if (conn.isClosed()){
            return r;
        }
        conn.commit();
        return r;
    }

    private <R> R rollback(Connection conn, Exception e) {
        try {
            conn.rollback();
            throw new ShardException(e);
        } catch (SQLException e1) {
            throw new ShardException(e1);
        }
    }

}
