package net.thumbtack.kadoshnikov.sharding.framework.shard.impl;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import net.thumbtack.kadoshnikov.sharding.framework.util.Constants;
import net.thumbtack.kadoshnikov.sharding.framework.shard.ShardManager;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by kkadoshnikov on 8/24/15.
 */
public class RedisShardManagerConfigurator {

    private final RedisShardManagerUpdater updater;
    private String host;
    private long period;
    private String shardSetName;
    private String mapName;
    private final File confFile;

    public RedisShardManagerConfigurator(ShardManager manager, File confFile) {
        updater = new RedisShardManagerUpdater(this, manager);
        this.confFile = confFile;
        if (confFile != null && confFile.exists()) {
            configUpdater();
        } else {
            updateConfig(ConfigFactory.load());
        }
    }

    private void configUpdater() {
        ScheduledExecutorService configService = Executors.newSingleThreadScheduledExecutor();
        configService.scheduleAtFixedRate(() -> {
                    Config conf = ConfigFactory.parseFile(confFile);
                    updateConfig(conf);
                }, 0,
                ConfigFactory.load().getLong(Constants.REDIS_SHARD_MANAGER_CONFIGURATOR_CACHE_INVALIDATE_TIMEOUT), TimeUnit.SECONDS);
    }

    private void updateConfig(Config conf) {
        updateHost(conf);
        shardSetName = conf.getString(Constants.REDIS_SHARD_MANAGER_CONFIGURATOR_SHARD_SET_NAME);
        mapName = conf.getString(Constants.REDIS_SHARD_MANAGER_CONFIGURATOR_SHARD_MAPPING_NAME);
        updatePeriod(conf);
    }

    private void updateHost(Config conf) {
        String confHost = conf.getString(Constants.REDIS_SHARD_MANAGER_CONFIGURATOR_HOST);
        if (!confHost.equals(host)) {
            updater.changePool(confHost);
            host = confHost;
        }
    }

    private void updatePeriod(Config conf) {
        long confPeriod = conf.getLong(Constants.REDIS_SHARD_MANAGER_CONFIGURATOR_PERIOD);
        if (confPeriod != period) {
            updater.changePeriod(confPeriod);
            period = confPeriod;
        }
    }

    public String getHost() {
        return host;
    }

    public long getPeriod() {
        return period;
    }

    public String getShardSetName() {
        return shardSetName;
    }

    public String getMapName() {
        return mapName;
    }
}
