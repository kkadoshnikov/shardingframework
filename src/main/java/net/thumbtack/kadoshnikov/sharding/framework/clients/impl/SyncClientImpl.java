package net.thumbtack.kadoshnikov.sharding.framework.clients.impl;

import net.thumbtack.kadoshnikov.sharding.framework.util.ExecuteUnknownResult;
import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosureDecorator;
import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;
import net.thumbtack.kadoshnikov.sharding.framework.clients.SyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.shard.Shard;
import net.thumbtack.kadoshnikov.sharding.framework.shard.ShardManager;
import net.thumbtack.kadoshnikov.sharding.framework.util.ShardException;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by kkadoshnikov on 8/18/15.
 */
public class SyncClientImpl implements SyncClient {

    private ShardManager shardManager;
    private QueryClosureDecorator queryClosureDecorator;

    public SyncClientImpl(ShardManager shardManager, String driverClass, QueryClosureDecorator decorator) {
        try {
            Class.forName(driverClass).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            throw new ShardException("can't load " + driverClass);
        }
        queryClosureDecorator = decorator;
        this.shardManager = shardManager;
    }

    @Override
    public <T> Optional<T> selectSpec(String id, QueryClosure<T> queryClosure) {
        try (Connection conn = shardManager.getShard(id).get().getConnection()) {
            return Optional.ofNullable(decorate(queryClosure).call(conn));
        } catch (Exception e) {
            throw new ShardException(e);
        }
    }

    @Override
    public <T> Optional<T> select(QueryClosure<T> queryClosure) {
        for (Shard shard : shardManager.shards()) {
            Optional<T> t = searchInShard(decorate(queryClosure), shard);
            if (t.isPresent()) return t;
        }
        return Optional.empty();
    }

    @Override
    public <T> List<T> selectAll(QueryClosure<List<T>> queryClosure) {
        List<T> resultList = new ArrayList<>();
        for (Shard shard : shardManager.shards()) {
            selectInShard(decorate(queryClosure), resultList, shard);
        }
        return resultList;
    }

    @Override
    public Optional<Integer> execute(String shardName, QueryClosure<Integer> queryClosure) {
        return execute(shardManager.getShardByName(shardName).orElseThrow(ShardException::new), queryClosure);
    }

    @Override
    public Optional<Integer> executeSpec(String id, QueryClosure<Integer> queryClosure) {
        return execute(shardManager.getShard(id).orElseThrow(ShardException::new), queryClosure);
    }

    @Override
    public Optional<ExecuteUnknownResult> executeUnknownSpec(QueryClosure<Integer> queryClosure) {
        for (Shard shard : shardManager.shards()) {
            Optional<Integer> result = execute(shard, queryClosure);
            if (result.isPresent()) {
                return Optional.of(new ExecuteUnknownResult(shard.getName(), result.get()));
            }
        }
        return Optional.empty();
    }

    @Override
    public String balanceInsert(QueryClosure<Integer> queryClosure) {
        Shard shard = shardManager.getBalanceShard().orElseThrow(ShardException::new);
        execute(shard, queryClosure);
        return shard.getName();
    }

    private Optional<Integer> execute(Shard shard, QueryClosure<Integer> queryClosure) {
        try (Connection conn = shard.getConnection()) {
            int result = (decorate(queryClosure).call(conn));
            if (result == 0) {
                return Optional.empty();
            }
            else {
                return Optional.of(result);
            }
        } catch (Exception e) {
            throw new ShardException(e);
        }
    }

    private <T> QueryClosure<T> decorate(QueryClosure<T> queryClosure) {
        return queryClosureDecorator.decorate(queryClosure);
    }

    private <T> Optional<T> searchInShard(QueryClosure<T> queryClosure, Shard shard) {
        try (Connection conn = shard.getConnection()) {
            T t = queryClosure.call(conn);
            return Optional.ofNullable(t);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private <T> void selectInShard(QueryClosure<List<T>> queryClosure, List<T> resultList, Shard shard) {
        try (Connection conn = shard.getConnection()) {
            resultList.addAll(queryClosure.call(conn));
        } catch (Exception e) {
            throw new ShardException(e);
        }
    }

}
