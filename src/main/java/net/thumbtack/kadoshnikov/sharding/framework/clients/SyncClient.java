package net.thumbtack.kadoshnikov.sharding.framework.clients;

import net.thumbtack.kadoshnikov.sharding.framework.util.ExecuteUnknownResult;
import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;

import java.util.List;
import java.util.Optional;

/**
 * Created by kkadoshnikov on 8/21/15.
 */
public interface SyncClient {

    <T> Optional<T> selectSpec(String id, QueryClosure<T> queryClosure);

    <T> Optional<T> select(QueryClosure<T> queryClosure);

    <T> List<T> selectAll(QueryClosure<List<T>> queryClosure);

    Optional<Integer> execute(String shardName, QueryClosure<Integer> queryClosure);

    Optional<Integer> executeSpec(String id, QueryClosure<Integer> queryClosure);

    Optional<ExecuteUnknownResult> executeUnknownSpec(QueryClosure<Integer> queryClosure);

    String balanceInsert(QueryClosure<Integer> queryClosure);

}
