package net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select;

import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

/**
 * Created by kkadoshnikov on 8/28/15.
 */
public class Select extends BaseSelect<Select> {

    private String field;
    private String value;

    private Select(String field, String value) {
        this.field = field;
        this.value = value;
    }

    public static Select select(String field, String value) {
        return new Select(field, value);
    }

    @SuppressWarnings("unchecked")
    public QueryClosure<Map<String, Object>> execute() {
        return conn -> {
            try (Statement st = conn.createStatement();
                 ResultSet rs = st.executeQuery(buildQuery().toString())) {
                String[] fields = getFields(rs);
                if (rs.next()) {
                    return getResult(rs, fields);
                }
                return null;
            }
        };
    }

    @Override
    protected StringBuilder buildQuery() {
        StringBuilder query = new StringBuilder("SELECT * FROM `");
        query.append(table).append("`");
        query.append(" WHERE ").append(table).append(".").append(field).append(" = '").append(value).append("'");
        query.append(";");
        return query;
    }

}
