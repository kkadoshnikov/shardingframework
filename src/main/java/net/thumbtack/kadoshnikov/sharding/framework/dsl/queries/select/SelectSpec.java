package net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select;

import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

/**
 * Created by kkadoshnikov on 8/28/15.
 */
public class SelectSpec extends BaseSelect<SelectSpec> {

    private String id;
    private String idField;

    private SelectSpec(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public SelectSpec setIdField(String idField) {
        this.idField = idField;
        return this;
    }

    public static SelectSpec selectSpec(String id) {
        return new SelectSpec(id);
    }

    @SuppressWarnings("unchecked")
    public QueryClosure<Map<String, Object>> execute() {
        return conn -> {
            try (Statement st = conn.createStatement();
                 ResultSet rs = st.executeQuery(buildQuery().toString())) {
                String[] fields = getFields(rs);
                if (rs.next()) {
                    return getResult(rs, fields);
                }
                return null;
            }
        };
    }

    @Override
    protected StringBuilder buildQuery() {
        StringBuilder query = new StringBuilder("SELECT * FROM `");
        query.append(table).append("`");
        query.append(" WHERE ").append(idField).append(" = ").append(id);
        query.append(";");
        return query;
    }

}
