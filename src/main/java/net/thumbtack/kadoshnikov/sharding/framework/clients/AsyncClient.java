package net.thumbtack.kadoshnikov.sharding.framework.clients;

import net.thumbtack.kadoshnikov.sharding.framework.util.ExecuteUnknownResult;
import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * Created by kkadoshnikov on 8/21/15.
 */
public interface AsyncClient {

    <T> CompletableFuture<Optional<T>> selectSpec(String id, QueryClosure<T> queryClosure);

    <T> CompletableFuture<Optional<T>> select(QueryClosure<T> queryClosure);

    <T> CompletableFuture<List<T>> selectAll(QueryClosure<List<T>> queryClosure);

    CompletableFuture<Optional<Integer>> execute(String shardName, QueryClosure<Integer> queryClosure);

    CompletableFuture<Optional<Integer>> executeSpec(String id, QueryClosure<Integer> queryClosure);

    CompletableFuture<Optional<ExecuteUnknownResult>> executeUnknownSpec(QueryClosure<Integer> queryClosure);

    CompletableFuture<String> balanceInsert(QueryClosure<Integer> queryClosure);

}
