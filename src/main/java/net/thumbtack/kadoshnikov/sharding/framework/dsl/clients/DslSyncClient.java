package net.thumbtack.kadoshnikov.sharding.framework.dsl.clients;

import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.DslQuery;

import java.util.Optional;

/**
 * Created by kkadoshnikov on 8/27/15.
 */
public interface DslSyncClient {

    <T> Optional<T> execute(DslQuery query);

}
