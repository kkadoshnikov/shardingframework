package net.thumbtack.kadoshnikov.sharding.framework.shard.impl;

import net.thumbtack.kadoshnikov.sharding.framework.util.Constants;
import net.thumbtack.kadoshnikov.sharding.framework.util.PoolFactory;
import net.thumbtack.kadoshnikov.sharding.framework.shard.Shard;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.commons.pool.impl.GenericObjectPoolFactory;

import java.sql.Connection;

/**
 * Created by kkadoshnikov on 8/18/15.
 */
public class ShardImpl implements Shard {

    private ObjectPool pool;
    private String name;

    public ShardImpl(String name, String url, String user, String pass, GenericObjectPool.Config config) {
        this.name = name;
        org.apache.commons.pool.PoolableObjectFactory factory = new PoolFactory(url, user, pass);
        GenericObjectPoolFactory genericObjectPoolFactory = new GenericObjectPoolFactory(factory, config);
        pool = genericObjectPoolFactory.createPool();
    }

    public ShardImpl(String name, String url, String user, String pass) {
        this(name, url, user, pass, Constants.DEFAULT_POOL_CONFIG);
    }

    @Override
    public Connection getConnection() throws Exception {
        return (Connection) pool.borrowObject();
    }

    @Override
    public String getName() {
        return name;
    }

}
