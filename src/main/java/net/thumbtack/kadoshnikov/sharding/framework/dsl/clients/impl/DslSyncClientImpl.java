package net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.impl;

import net.thumbtack.kadoshnikov.sharding.framework.clients.SyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.DslHandler;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.DslSyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.*;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select.Select;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select.SelectAll;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select.SelectSpec;
import net.thumbtack.kadoshnikov.sharding.framework.shard.ShardManager;
import net.thumbtack.kadoshnikov.sharding.framework.util.DSLException;
import net.thumbtack.kadoshnikov.sharding.framework.util.ExecuteUnknownResult;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by kkadoshnikov on 8/27/15.
 */
public class DslSyncClientImpl implements DslSyncClient {

    private String idField;
    private SyncClient syncClient;
    private ShardManager shardManager;
    private Map<Class, DslHandler> handlers;

    public DslSyncClientImpl(String idField, SyncClient syncClient, ShardManager shardManager, Map<Class, DslHandler> handlers) {
        if (idField.isEmpty()) {
            throw new DSLException("idField is empty");
        }
        this.idField = idField;
        this.syncClient = syncClient;
        this.shardManager = shardManager;
        this.handlers = handlers;
        handlers.put(SelectSpec.class, this::selectSpec);
        handlers.put(Select.class, this::select);
        handlers.put(SelectAll.class, this::selectAll);
        handlers.put(Insert.class, this::insert);
        handlers.put(Update.class, this::update);
        handlers.put(Delete.class, this::delete);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> Optional<T> execute(DslQuery query) {
        return handlers.get(query.getClass()).handle(query, shardManager, syncClient);
    }

    private Optional<Map<String, Object>> selectSpec(DslQuery dslQuery, ShardManager shardManager, SyncClient syncClient) {
        SelectSpec select = (SelectSpec) dslQuery;
        return syncClient.selectSpec(select.getId(), select.setIdField(idField).execute());
    }

    private Optional<Map<String, Object>> select(DslQuery dslQuery, ShardManager shardManager, SyncClient syncClient) {
        Select select = (Select) dslQuery;
        return syncClient.select(select.execute());
    }

    private Optional<List<Map<String, Object>>> selectAll(DslQuery dslQuery, ShardManager shardManager, SyncClient syncClient) {
        SelectAll select = (SelectAll) dslQuery;
        return Optional.of(syncClient.selectAll(select.setIdField(idField).execute()));
    }

    private Optional<Integer> insert(DslQuery dslQuery, ShardManager shardManager, SyncClient syncClient) {
        Insert insert = (Insert) dslQuery;
        if (insert.getShard().isEmpty()) {
            return balanceInsert(insert);
        }
        else {
            return insertSpec(insert.getShard(), insert);
        }
    }

    private Optional<Integer> update(DslQuery dslQuery, ShardManager shardManager, SyncClient syncClient) {
        Update update = (Update) dslQuery;
        if (update.getShard().isEmpty()) {
            return Optional.of(syncClient.executeUnknownSpec(update.setIdField(idField).execute()).
                    orElseThrow(DSLException::new).getChangedRowCount());
        }
        else {
            return Optional.of(syncClient.execute(update.getShard(), update.setIdField(idField).execute()).
                    orElseThrow(DSLException::new));

        }
    }

    private Optional<Integer> delete(DslQuery dslQuery, ShardManager shardManager, SyncClient syncClient) {
        Delete delete = (Delete) dslQuery;
        if (delete.getShard().isEmpty()) {
            return delete(delete);
        }
        else {
            return deleteSpec(delete.getShard(), delete);
        }
    }

    private Optional<Integer> insertSpec(String shardName, Insert insert) {
        String id = insert.getId(idField);
        syncClient.execute(shardName, insert.execute()).orElseThrow(DSLException::new);
        shardManager.addId(shardName, id);
        return Optional.of(1);
    }

    private Optional<Integer> balanceInsert(Insert insert) {
        String id = insert.getId(idField);
        String shardName = syncClient.balanceInsert(insert.execute());
        shardManager.addId(shardName, id);
        return Optional.of(1);
    }

    private Optional<Integer> deleteSpec(String shardName, Delete delete) {
        delete.setIdField(idField);
        String id = delete.getId();
        syncClient.execute(shardName, delete.setIdField(idField).execute()).orElseThrow(DSLException::new);
        shardManager.removeId(id);
        return Optional.of(1);
    }

    private Optional<Integer> delete(Delete delete) {
        String id = delete.getId();
        ExecuteUnknownResult result = syncClient.executeUnknownSpec(delete.setIdField(idField).execute()).orElseThrow(DSLException::new);
        shardManager.removeId(id);
        return Optional.of(result.getChangedRowCount());
    }

}
