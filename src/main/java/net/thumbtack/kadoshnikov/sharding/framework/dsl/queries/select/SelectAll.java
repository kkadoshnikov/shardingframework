package net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select;

import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kkadoshnikov on 8/28/15.
 */
public class SelectAll extends BaseSelect<SelectAll> {

    private List<String> ids;
    private String idField;

    private SelectAll(List<String> ids) {
        this.ids = ids;
    }

    public SelectAll setIdField(String idField) {
        this.idField = idField;
        return this;
    }

    public static SelectAll selectAll(List<String> ids) {
        return new SelectAll(ids);
    }

    @SuppressWarnings("unchecked")
    public QueryClosure<List<Map<String, Object>>> execute() {
        return conn -> {
            try (Statement st = conn.createStatement();
                 ResultSet rs = st.executeQuery(buildQuery().toString())) {
                String[] fields = getFields(rs);
                List<Map<String, Object>> resultList = new ArrayList<>();
                while (rs.next()) {
                    resultList.add(getResult(rs, fields));
                }
                return resultList;
            }
        };
    }

    @Override
    protected StringBuilder buildQuery() {
        StringBuilder query = new StringBuilder("SELECT * FROM `");
        query.append(table).append("`");
        query.append(" WHERE `").append(idField).append("` IN (").append(idsToString(ids)).append(")");
        query.append(";");
        return query;
    }

    private String idsToString(List<String> ids) {
        StringBuilder idsSrt = new StringBuilder();
        for (String id : ids) {
            idsSrt.append(id).append(",");
        }
        idsSrt.deleteCharAt(idsSrt.length() - 1);
        return idsSrt.toString();
    }

}
