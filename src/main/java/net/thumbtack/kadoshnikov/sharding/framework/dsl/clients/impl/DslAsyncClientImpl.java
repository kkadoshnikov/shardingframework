package net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.impl;

import net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.DslAsyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.DslSyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.DslQuery;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.Insert;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.Query;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select.Select;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select.SelectAll;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select.SelectSpec;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * Created by kkadoshnikov on 8/31/15.
 */
public class DslAsyncClientImpl implements DslAsyncClient {

    private DslSyncClient client;

    public DslAsyncClientImpl(DslSyncClient client) {
        this.client = client;
    }

    @Override
    public <T> CompletableFuture<Optional<T>> execute(DslQuery query) {
        return CompletableFuture.supplyAsync(() -> client.execute(query));
    }

}
