package net.thumbtack.kadoshnikov.sharding.framework.util;

/**
 * Created by kkadoshnikov on 8/31/15.
 */
public class ClientException extends RuntimeException {

    public ClientException(Throwable cause) {
        super(cause);
    }

}
