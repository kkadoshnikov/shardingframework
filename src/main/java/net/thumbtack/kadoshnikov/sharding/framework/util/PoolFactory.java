package net.thumbtack.kadoshnikov.sharding.framework.util;

/**
 * Created by kkadoshnikov on 8/18/15.
 */

import java.sql.DriverManager;

import org.apache.commons.pool.BasePoolableObjectFactory;

public class PoolFactory extends BasePoolableObjectFactory {
    private final String url;
    private final String user;
    private final String password;

    public PoolFactory(String url, String user, String password) {
        this.url = url + "?autoReconnectForPools=true";
        this.user = user;
        this.password = password;
    }

    @Override
    public Object makeObject() throws Exception {
        return DriverManager.getConnection(url, user, password);
    }
}
