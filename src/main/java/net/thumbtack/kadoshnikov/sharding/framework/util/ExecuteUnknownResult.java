package net.thumbtack.kadoshnikov.sharding.framework.util;

/**
 * Created by kkadoshnikov on 8/31/15.
 */
public class ExecuteUnknownResult {
    private final String shard;
    private final int changedRowCount;

    public ExecuteUnknownResult(String shard, int changedRowCount) {
        this.shard = shard;
        this.changedRowCount = changedRowCount;
    }

    public int getChangedRowCount() {
        return changedRowCount;
    }

    public String getShard() {
        return shard;
    }
}
