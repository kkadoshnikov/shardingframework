package net.thumbtack.kadoshnikov.sharding.framework.util;

/**
 * Created by kkadoshnikov on 8/18/15.
 */
public class ShardException extends RuntimeException {

    public ShardException(){
        super("shard not found");
    }

    public ShardException(String message) {
        super(message);
    }

    public ShardException(Throwable cause) {
        super(cause);
    }

}
