package net.thumbtack.kadoshnikov.sharding.framework.dsl.queries;

import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;

/**
 * Created by kkadoshnikov on 8/31/15.
 */
public interface DslQuery {

    <T> QueryClosure<T> execute();

}
