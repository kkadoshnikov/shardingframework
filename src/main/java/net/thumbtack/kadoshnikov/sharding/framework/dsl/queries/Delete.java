package net.thumbtack.kadoshnikov.sharding.framework.dsl.queries;

import net.thumbtack.kadoshnikov.sharding.framework.util.QueryClosure;

import java.sql.Statement;

/**
 * Created by kkadoshnikov on 8/27/15.
 */
public class Delete extends Query<Delete> {

    private String id;
    private String table;
    private String idField;

    private Delete(String id) {
        this.id = id;
    }

    public Delete setIdField(String idField) {
        this.idField = idField;
        return this;
    }

    public static Delete delete(String id) {
        return new Delete(id);
    }

    public Delete from(String table) {
        this.table = table;
        return this;
    }

    public QueryClosure<Integer> execute() {
        return conn -> {
            try (Statement st = conn.createStatement()) {
                return st.executeUpdate(buildQuery().toString());
            }
        };
    }

    protected StringBuilder buildQuery() {
        StringBuilder query = new StringBuilder("DELETE FROM `");
        query.append(table).append("`");
        query.append(" WHERE ").append(idField).append(" = ").append(id);
        query.append(";");
        return query;
    }

    public String getId() {
        return id;
    }

}
