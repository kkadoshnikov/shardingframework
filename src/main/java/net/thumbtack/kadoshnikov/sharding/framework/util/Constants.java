package net.thumbtack.kadoshnikov.sharding.framework.util;

import org.apache.commons.pool.impl.GenericObjectPool;

/**
 * Created by kkadoshnikov on 8/20/15.
 */
public class Constants {

    public static final String DEFAULT_SHARD_SET_NAME = "shards";
    public static final String MAP_DEFAULT_NAME = "shardMap";

    public static final GenericObjectPool.Config DEFAULT_POOL_CONFIG;
    public static final String URL_KEY = "url";
    public static final String USER_KEY = "user";
    public static final String PASS_KEY = "pass";
    public static final String REDIS_SHARD_MANAGER_CONFIGURATOR_CACHE_INVALIDATE_TIMEOUT = "redisShardManagerConfigurator.cacheInvalidateTimeout";
    public static final String REDIS_SHARD_MANAGER_CONFIGURATOR_HOST = "redisShardManagerConfigurator.host";
    public static final String REDIS_SHARD_MANAGER_CONFIGURATOR_SHARD_SET_NAME = "redisShardManagerConfigurator.shardSetName";
    public static final String REDIS_SHARD_MANAGER_CONFIGURATOR_SHARD_MAPPING_NAME = "redisShardManagerConfigurator.shardMappingName";
    public static final String REDIS_SHARD_MANAGER_CONFIGURATOR_PERIOD = "redisShardManagerConfigurator.period";

    static {
        DEFAULT_POOL_CONFIG = new GenericObjectPool.Config();
        DEFAULT_POOL_CONFIG.maxActive = 100;
        DEFAULT_POOL_CONFIG.testOnBorrow = true;
        DEFAULT_POOL_CONFIG.testWhileIdle = true;
        DEFAULT_POOL_CONFIG.timeBetweenEvictionRunsMillis = 10000;
        DEFAULT_POOL_CONFIG.minEvictableIdleTimeMillis = 60000;
    }

}
