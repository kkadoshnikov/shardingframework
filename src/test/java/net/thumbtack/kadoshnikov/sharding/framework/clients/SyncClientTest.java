package net.thumbtack.kadoshnikov.sharding.framework.clients;

import net.thumbtack.kadoshnikov.sharding.framework.clients.impl.RedisClientBuilder;
import net.thumbtack.kadoshnikov.sharding.framework.shard.impl.ShardManagerImpl;
import net.thumbtack.kadoshnikov.sharding.framework.util.ClientUtil;
import net.thumbtack.kadoshnikov.sharding.framework.util.ShardException;
import net.thumbtack.kadoshnikov.sharding.framework.util.User;
import org.junit.*;

import java.util.*;

import static net.thumbtack.kadoshnikov.sharding.framework.util.ClientUtil.*;
import static net.thumbtack.kadoshnikov.sharding.framework.util.ClientUtil.insertQueryClosure;

/**
 * Created by kkadoshnikov on 8/18/15.
 */
public class SyncClientTest {

    private static final SyncClient client = new RedisClientBuilder().withDriver("com.mysql.jdbc.Driver").
    withShardManager(new ShardManagerImpl()).withTransaction().sync();

    @BeforeClass
    public static void init() throws Exception {
        ClientUtil.init();
        Thread.sleep(1000);
    }

    @AfterClass
    public static void close() {
        ClientUtil.close();
    }

    @Test(expected = ShardException.class)
    public void testBadDriver() {
        new RedisClientBuilder().withDriver("badDriver").withShardManager(new ShardManagerImpl()).sync();
    }

    @Test
    public void testSelectSpec() throws InterruptedException {
        Assert.assertEquals(new User("2", "jhon", "ho-ho"), selectSpec("2").get());
    }

    @Test
    public void testSelect() throws InterruptedException {
        Assert.assertEquals(new User("2", "jhon", "ho-ho"), select("jhon").get());
    }

    @Test
    public void testBadSelect() throws InterruptedException {
        Assert.assertEquals(Optional.empty(), select("jho"));
    }

    @Test
    public void testSelectAll() throws InterruptedException {
        User[] actual = new User[getIds().size()];
        actual = selectAll(getIds("1", "2")).toArray(actual);
        Arrays.sort(actual);
        Assert.assertArrayEquals(getSelectAllExpected(), actual);
    }

    @Test
    public void testInsert() {
        Assert.assertEquals(1, (int) client.execute("shard1", insertQueryClosure("10")).get());
        client.execute("shard1", deleteQueryClosure("10"));
    }

    @Test
    public void testDelete() {
        client.execute("shard2", insertQueryClosure("10"));
        Assert.assertEquals("shard2", client.executeUnknownSpec(deleteQueryClosure("10")).get().getShard());
    }

    private Optional<User> selectSpec(String userId) {
        return client.selectSpec(userId, selectSpecQueryClosure(userId));
    }

    private Optional<User> select(String userName) {
        return client.select(selectQueryClosure(userName));
    }

    private List<User> selectAll(List<String> ids) {
        return client.selectAll(selectAllQueryClosure(ids));
    }

}
