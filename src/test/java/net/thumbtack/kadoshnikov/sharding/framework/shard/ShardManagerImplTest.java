package net.thumbtack.kadoshnikov.sharding.framework.shard;

/**
 * Created by kkadoshnikov on 8/21/15.
 */


import net.thumbtack.kadoshnikov.sharding.framework.shard.impl.RedisShardManagerConfigurator;
import net.thumbtack.kadoshnikov.sharding.framework.shard.impl.ShardManagerImpl;
import net.thumbtack.kadoshnikov.sharding.framework.util.Constants;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static net.thumbtack.kadoshnikov.sharding.framework.util.ClientUtil.*;
import static net.thumbtack.kadoshnikov.sharding.framework.util.RedisUtil.*;

public class ShardManagerImplTest {

    private ShardManagerImpl shardManager;

    @Before
    public void before() {
        shardManager = new ShardManagerImpl();
        new RedisShardManagerConfigurator(shardManager, null);
    }

    @After
    public void after() {
        del(Constants.DEFAULT_SHARD_SET_NAME);
        delMapValue(Constants.MAP_DEFAULT_NAME, "shard1");
        delMapValue(Constants.MAP_DEFAULT_NAME, "5");
    }

    @Test
    public void testGetShards() throws InterruptedException {
        putShard("shard1", "jdbc:mysql://localhost:3306/shard1", "root", "1");
        putShard("shard2", "jdbc:mysql://localhost:3306/shard2", "root", "1");
        addAllToSet(Constants.DEFAULT_SHARD_SET_NAME, "shard1", "shard2");
        Thread.sleep(1000);
        Assert.assertEquals(2, shardManager.shards().size());
        delMapValue(Constants.MAP_DEFAULT_NAME, "shard2");
    }

    @Test
    public void testDeleteShard() throws InterruptedException {
        addShard();
        delInSet(Constants.DEFAULT_SHARD_SET_NAME, "shard1");
        Thread.sleep(1000);
        for (Shard shard : shardManager.shards()) {
            System.out.println(shard.getName());
        }
        Assert.assertEquals(0, shardManager.shards().size());
    }

    @Test
    public void testGetMapValue() throws InterruptedException {
        addShard();
        Assert.assertNotNull(shardManager.getShard("5").get());
    }

    @Test
    public void testDelMapValue() throws InterruptedException {
        addShard();
        delMapValue(Constants.MAP_DEFAULT_NAME, "5");
        Thread.sleep(1000);
        Assert.assertEquals(Optional.empty(), shardManager.getShard("5"));
    }

    private void addShard() throws InterruptedException {
        putShard("shard1", "jdbc:mysql://localhost:3306/shard1", "root", "1");
        addAllToSet(Constants.DEFAULT_SHARD_SET_NAME, "shard1");
        setMapValue(Constants.MAP_DEFAULT_NAME, "5", "shard1");
        Thread.sleep(1000);
    }

}
