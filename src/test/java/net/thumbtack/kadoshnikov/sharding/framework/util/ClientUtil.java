package net.thumbtack.kadoshnikov.sharding.framework.util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.*;

import static net.thumbtack.kadoshnikov.sharding.framework.util.RedisUtil.*;

/**
 * Created by kkadoshnikov on 8/19/15.
 */
public class ClientUtil {

    public static void init() throws Exception {
        putShard("shard1", "jdbc:mysql://localhost:3306/shard1", "root", "1");
        putShard("shard2", "jdbc:mysql://localhost:3306/shard2", "root", "1");
        addAllToSet(Constants.DEFAULT_SHARD_SET_NAME, "shard1", "shard2");
        setMapValue(Constants.MAP_DEFAULT_NAME, "1", "shard2");
        setMapValue(Constants.MAP_DEFAULT_NAME, "2", "shard1");
    }

    public static void close() {
        delMapValue(Constants.MAP_DEFAULT_NAME, "shard1");
        delMapValue(Constants.MAP_DEFAULT_NAME, "shard2");
        del(Constants.DEFAULT_SHARD_SET_NAME);
    }

    public static User[] getSelectAllExpected() {
        User[] expected = new User[2];
        expected[0] = new User("1", "olya", "info");
        expected[1] = new User("2", "jhon", "ho-ho");
        return expected;
    }

    public static List<String> getIds(String... ids) {
        List<String> result = new ArrayList<>();
        Collections.addAll(result, ids);
        return result;
    }

    public static QueryClosure<User> selectSpecQueryClosure(String userId) {
        return conn -> {
            try (Statement st = conn.createStatement();
                 ResultSet rs = st.executeQuery("select * from user WHERE idUser = " + userId)) {
                rs.next();
                return new User(rs.getString(1), rs.getString(2), rs.getString(3));
            }
        };
    }

    public static QueryClosure<User> selectQueryClosure(String userName) {
        return conn -> {
            try (Statement st = conn.createStatement();
                 ResultSet rs = st.executeQuery("select * from user WHERE name = " + "'" + userName + "'")) {
                rs.next();

                return new User(rs.getString(1), rs.getString(2), rs.getString(3));
            }
        };
    }

    public static QueryClosure<List<User>> selectAllQueryClosure(List<String> ids) {
        return conn -> {
            try (Statement st = conn.createStatement();
                 ResultSet rs = st.executeQuery("select * from user WHERE `idUser` IN (" + idsToString(ids) + ");")) {
                List<User> resultList = new ArrayList<>();
                while (rs.next()) {
                    resultList.add(new User(rs.getString(1), rs.getString(2), rs.getString(3)));
                }
                return resultList;
            }
        };
    }

    public static QueryClosure<Integer> insertQueryClosure(String id) {
        return conn -> {
            try (Statement st = conn.createStatement()) {
                return st.executeUpdate("INSERT INTO `user` (`idUser`, `name`, `info`) " +
                        "VALUES ('" + id + "', 'name', 'info');");
            }
        };

    }

    public static QueryClosure<Integer> deleteQueryClosure(String id) {
        return conn -> {
            try (Statement st = conn.createStatement()) {
                return st.executeUpdate("DELETE FROM  `user` WHERE idUser = " + id);
            }
        };
    }

    private static StringBuilder idsToString(List<String> ids) {
        StringBuilder idsSrt = new StringBuilder();
        for (String id : ids) {
            idsSrt.append(id).append(",");
        }
        idsSrt.deleteCharAt(idsSrt.length() - 1);
        return idsSrt;
    }

    public static void putShard(String name, String url, String user, String pass) {
        Map<String, String> fields = new HashMap<>();
        fields.put("url", url);
        fields.put("user", user);
        fields.put("pass", pass);
        setMapValues(name, fields);
    }

}
