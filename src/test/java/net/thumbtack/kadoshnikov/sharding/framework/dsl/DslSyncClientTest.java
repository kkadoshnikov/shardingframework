package net.thumbtack.kadoshnikov.sharding.framework.dsl;

import net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.impl.DslClientBuilder;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.DslSyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.shard.impl.ShardManagerImpl;
import net.thumbtack.kadoshnikov.sharding.framework.util.ClientUtil;
import net.thumbtack.kadoshnikov.sharding.framework.util.DSLException;
import net.thumbtack.kadoshnikov.sharding.framework.util.ShardException;
import net.thumbtack.kadoshnikov.sharding.framework.util.User;
import org.junit.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.Delete.*;
import static net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.Insert.insert;
import static net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.Update.update;
import static net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select.Select.select;
import static net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select.SelectAll.selectAll;
import static net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select.SelectSpec.*;
import static net.thumbtack.kadoshnikov.sharding.framework.util.ClientUtil.*;

/**
 * Created by kkadoshnikov on 8/28/15.
 */
public class DslSyncClientTest {

    private static final DslSyncClient client = new DslClientBuilder().withDriver("com.mysql.jdbc.Driver").
            withShardManager(new ShardManagerImpl()).withTransaction().withIdField("idUser").sync();

    @BeforeClass
    public static void init() throws Exception {
        ClientUtil.init();
        Thread.sleep(1000);
    }

    @AfterClass
    public static void close() {
        ClientUtil.close();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testInsertAndSelectSpec() {
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("10", "name", "info"));
        User actual = new User((Map<String, Object>) client.execute(selectSpec("10").from("user")).get());
        Assert.assertEquals(new User("10", "name", "info"), actual);
        client.execute(delete("10").shard("shard1").from("user"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testBalanceInsertAndSelect() {
        client.execute(insert("idUser", "name", "info").into("user").values("10", "name", "info"));
        User actual = new User((Map<String, Object>) client.execute(select("name", "name").from("user")).get());
        Assert.assertEquals(new User("10", "name", "info"), actual);
        client.execute(delete("10").from("user"));
    }

    @Test()
    public void testDeleteUnknownSpec() {
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("10", "name", "info"));
        client.execute(delete("10").from("user"));
        Assert.assertFalse(client.execute(select("name", "name").from("user")).isPresent());
    }

    @Test(expected = ShardException.class)
    public void testDelete() {
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("10", "name", "info"));
        client.execute(delete("10").shard("shard1").from("user"));
        client.execute(selectSpec("10").from("user")).get();
    }

    @Test(expected = DSLException.class)
    public void testDeleteNonexistent() {
        client.execute(delete("150").shard("shard1").from("user"));
    }

    @Test(expected = DSLException.class)
    public void testDeleteNonexistent1() {
        client.execute(delete("150").from("user"));
    }

    @Test
    public void testSelectNonexistent() {
        Assert.assertFalse(client.execute(select("name", "badValue").from("user")).isPresent());
    }

    @Test(expected = ShardException.class)
    public void testSelectSpecNonexistent() {
        client.execute(selectSpec("150")).isPresent();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSelectAll() {
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("10", "name", "info"));
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("11", "name1", "info"));
        User[] actual = new User[2];
        actual = User.createUsers((List<Map<String, Object>>)
                client.execute(selectAll(getIds("10", "11")).from("user")).get()).toArray(actual);
        Arrays.sort(actual);
        User[] expected = {new User("10", "name", "info"), new User("11", "name1", "info")};
        Assert.assertArrayEquals(expected, actual);
        client.execute(delete("10").shard("shard1").from("user"));
        client.execute(delete("11").shard("shard1").from("user"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testUpdate() {
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("10", "name", "info"));
        client.execute(update("10").from("user").shard("shard1").set("name", "update"));
        Map<String, Object> map = (Map<String, Object>) client.execute( select("name", "update").from("user")).get();
        Assert.assertEquals("10", map.get("idUser"));
        client.execute(delete("10").shard("shard1").from("user"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testUpdate1() {
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("10", "name", "info"));
        client.execute(update().shard("shard1").from("user").where("idUser = 10").set("name", "update"));
        Map<String, Object> map = (Map<String, Object>) client.execute( select("name", "update").from("user")).get();
        Assert.assertEquals("10", map.get("idUser"));
        client.execute(delete("10").shard("shard1").from("user"));
    }

}
