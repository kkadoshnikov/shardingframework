package net.thumbtack.kadoshnikov.sharding.framework.clients;

/**
 * Created by kkadoshnikov on 8/19/15.
 */

import net.thumbtack.kadoshnikov.sharding.framework.clients.impl.RedisClientBuilder;
import net.thumbtack.kadoshnikov.sharding.framework.shard.impl.ShardManagerImpl;
import net.thumbtack.kadoshnikov.sharding.framework.util.ClientUtil;
import net.thumbtack.kadoshnikov.sharding.framework.util.User;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static net.thumbtack.kadoshnikov.sharding.framework.util.ClientUtil.*;

public class AsyncClientTest {

    private final static AsyncClient asyncClient = new RedisClientBuilder().withDriver("com.mysql.jdbc.Driver").
            withShardManager(new ShardManagerImpl()).async();

    @BeforeClass
    public static void init() throws Exception {
        ClientUtil.init();
        Thread.sleep(1000);
    }

    @AfterClass
    public static void close() {
        ClientUtil.close();
    }

    @Test
    public void testSelectSpec() throws Exception {
        Assert.assertEquals(new User("2", "jhon", "ho-ho"), selectSpec("2").get().get());
    }

    @Test
    public void testSelect() throws Exception {
        Assert.assertEquals(new User("2", "jhon", "ho-ho"), select("jhon").get().get());
    }

    @Test
    public void testSelectAll() throws Exception {
        User[] actual = new User[ClientUtil.getIds().size()];
        actual = selectAll(ClientUtil.getIds("1", "2")).get().toArray(actual);
        Arrays.sort(actual);
        Assert.assertArrayEquals(getSelectAllExpected(), actual);
    }

    @Test
    public void testInsert() throws ExecutionException, InterruptedException {
        Assert.assertEquals(1, (int) asyncClient.execute("shard1", ClientUtil.insertQueryClosure("10")).get().get());
        asyncClient.execute("shard1", deleteQueryClosure("10")).get();
    }

    @Test
    public void testDelete() throws ExecutionException, InterruptedException {
        asyncClient.execute("shard2", ClientUtil.insertQueryClosure("10")).get();
        Assert.assertEquals("shard2", asyncClient.executeUnknownSpec(deleteQueryClosure("10")).get().get().getShard());
    }

    private CompletableFuture<Optional<User>> selectSpec(String userId) {
        return asyncClient.selectSpec(userId, selectSpecQueryClosure(userId));
    }

    private CompletableFuture<Optional<User>> select(String userName) {
        return asyncClient.select(selectQueryClosure(userName));
    }

    private CompletableFuture<List<User>> selectAll(List<String> ids) {
        return asyncClient.selectAll(selectAllQueryClosure(ids));
    }

}
