package net.thumbtack.kadoshnikov.sharding.framework.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Map;

/**
 * Created by kkadoshnikov on 8/24/15.
 */
public class RedisUtil {

    private final static JedisPool pool;

    static {
        Config conf = ConfigFactory.load();
        pool = new JedisPool(conf.getString("redisShardManagerConfigurator.host"));
    }

    public static void del(String key) {
        try (Jedis jedis = pool.getResource()) {
            jedis.del(key);
        }
    }

    public static void setMapValue(String key, String field, String value) {
        try (Jedis jedis = pool.getResource()) {
            jedis.hset(key, field, value);
        }
    }

    public static void setMapValues(String key, Map<String, String> map) {
        try (Jedis jedis = pool.getResource()) {
            jedis.hmset(key, map);
        }
    }

    public static void delMapValue(String key, String field) {
        try (Jedis jedis = pool.getResource()) {
            jedis.hdel(key, field);
        }
    }

    public static void addAllToSet(String key, String... values) {
        try (Jedis jedis = pool.getResource()) {
            jedis.sadd(key, values);
        }
    }

    public static void delInSet(String key, String... values) {
        try (Jedis jedis = pool.getResource()) {
            jedis.srem(key, values);
        }
    }


}
