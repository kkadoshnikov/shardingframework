package net.thumbtack.kadoshnikov.sharding.framework.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by kkadoshnikov on 8/18/15.
 */
public class User implements Comparable {

    private final String id;
    private final String name;
    private final String info;

    public User(String id, String name, String info) {
        this.id = id;
        this.name = name;
        this.info = info;
    }

    public User(Map<String, Object> map) {
        id = (String) map.get("idUser");
        name = (String) map.get("name");
        info = (String) map.get("info");
    }

    static public List<User> createUsers (List<Map<String, Object>> users) {
        return users.stream().map(User::new).collect(Collectors.toList());
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", name='" + name + '\'' +
                ", info='" + info + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!id.equals(user.id)) return false;
        if (!name.equals(user.name)) return false;
        return info.equals(user.info);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + info.hashCode();
        return result;
    }


    @Override
    public int compareTo(Object o) {
        return id.compareTo(((User) o).getId());
    }
}
