package net.thumbtack.kadoshnikov.sharding.framework.dsl;

import net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.DslAsyncClient;
import net.thumbtack.kadoshnikov.sharding.framework.dsl.clients.impl.DslClientBuilder;
import net.thumbtack.kadoshnikov.sharding.framework.shard.impl.ShardManagerImpl;
import net.thumbtack.kadoshnikov.sharding.framework.util.ClientUtil;
import net.thumbtack.kadoshnikov.sharding.framework.util.DSLException;
import net.thumbtack.kadoshnikov.sharding.framework.util.ShardException;
import net.thumbtack.kadoshnikov.sharding.framework.util.User;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.Delete.delete;
import static net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.Insert.insert;
import static net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.Update.update;
import static net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select.Select.select;
import static net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select.SelectAll.selectAll;
import static net.thumbtack.kadoshnikov.sharding.framework.dsl.queries.select.SelectSpec.selectSpec;
import static net.thumbtack.kadoshnikov.sharding.framework.util.ClientUtil.getIds;

/**
 * Created by kkadoshnikov on 8/31/15.
 */
public class DslAsyncClientTest {

    private static final DslAsyncClient client = new DslClientBuilder().withDriver("com.mysql.jdbc.Driver").
            withShardManager(new ShardManagerImpl()).withTransaction().withIdField("idUser").async();

    @BeforeClass
    public static void init() throws Exception {
        ClientUtil.init();
        Thread.sleep(1000);
    }

    @AfterClass
    public static void close() {
        ClientUtil.close();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testInsertAndSelectSpec() throws ExecutionException, InterruptedException {
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("10", "name", "info")).get();
        User actual = new User((Map<String, Object>) client.execute(selectSpec("10").from("user")).get().get());
        Assert.assertEquals(new User("10", "name", "info"), actual);
        client.execute(delete("10").shard("shard1").from("user")).get();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testBalanceInsertAndSelect() throws ExecutionException, InterruptedException {
        client.execute(insert("idUser", "name", "info").into("user").values("10", "name", "info")).get();
        User actual = new User((Map<String, Object>) client.execute(select("name", "name").from("user")).get().get());
        Assert.assertEquals(new User("10", "name", "info"), actual);
        client.execute(delete("10").from("user")).get();
    }

    @Test()
    public void testDeleteUnknownSpec() throws ExecutionException, InterruptedException {
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("10", "name", "info")).get();
        client.execute(delete("10").from("user")).get();
        Assert.assertFalse(client.execute(select("name", "name").from("user")).get().isPresent());
    }

    @Test(expected = ShardException.class)
    public void testDelete() throws Throwable {
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("10", "name", "info")).get();
        client.execute(delete("10").shard("shard1").from("user")).get();
        try {
            client.execute(selectSpec("10").from("user")).get().get();
        } catch (ExecutionException e) {
            throw e.getCause();
        }
    }

    @Test(expected = DSLException.class)
    public void testDeleteNonexistent() throws Throwable {
        try {
            client.execute(delete("150").shard("shard1").from("user")).get();
        } catch (ExecutionException e) {
            throw e.getCause();
        }
    }

    @Test(expected = DSLException.class)
    public void testDeleteNonexistent1() throws Throwable {
        try {
            client.execute(delete("150").from("user")).get();
        } catch (ExecutionException e) {
            throw e.getCause();
        }
    }

    @Test
    public void testSelectNonexistent() throws ExecutionException, InterruptedException {
        Assert.assertFalse(client.execute(select("name", "badValue").from("user")).get().isPresent());
    }

    @Test(expected = ShardException.class)
    public void testSelectSpecNonexistent() throws Throwable {
        try {
            client.execute(selectSpec("150")).get().isPresent();
        } catch (ExecutionException e) {
            throw e.getCause();
        }
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSelectAll() throws ExecutionException, InterruptedException {
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("10", "name", "info")).get();
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("11", "name1", "info")).get();
        User[] actual = new User[2];
        actual = User.createUsers((List<Map<String, Object>>)
                client.execute(selectAll(getIds("10", "11")).from("user")).get().get()).toArray(actual);
        Arrays.sort(actual);
        User[] expected = {new User("10", "name", "info"), new User("11", "name1", "info")};
        Assert.assertArrayEquals(expected, actual);
        client.execute(delete("10").shard("shard1").from("user")).get();
        client.execute(delete("11").shard("shard1").from("user")).get();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testUpdate() throws ExecutionException, InterruptedException {
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("10", "name", "info")).get();
        client.execute(update("10").from("user").shard("shard1").set("name", "update")).get();
        Map<String, Object> map = (Map<String, Object>) client.execute( select("name", "update").from("user")).get().get();
        Assert.assertEquals("10", map.get("idUser"));
        client.execute(delete("10").shard("shard1").from("user")).get();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testUpdate1() throws ExecutionException, InterruptedException {
        client.execute(insert("idUser", "name", "info").shard("shard1").into("user").values("10", "name", "info")).get();
        client.execute(update().shard("shard1").from("user").where("idUser = 10").set("name", "update")).get();
        Map<String, Object> map = (Map<String, Object>) client.execute( select("name", "update").from("user")).get().get();
        Assert.assertEquals("10", map.get("idUser"));
        client.execute(delete("10").shard("shard1").from("user")).get();
    }

}
