package net.thumbtack.kadoshnikov.sharding.framework.shard;

import net.thumbtack.kadoshnikov.sharding.framework.shard.impl.RedisShardManagerConfigurator;
import net.thumbtack.kadoshnikov.sharding.framework.shard.impl.ShardManagerImpl;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by kkadoshnikov on 8/25/15.
 */
public class RedisShardManagerConfiguratorTest {

    private static final String CONFIG_FILE_NAME = "/home/kkadoshnikov/sharding/src/test/resources/application.conf";
    private final RedisShardManagerConfigurator configurator = new RedisShardManagerConfigurator(new ShardManagerImpl(),
            new File(CONFIG_FILE_NAME));

    private final String oldConf = "redisShardManagerConfigurator {\n" +
            "            host=\"localhost\"\n" +
            "            shardSetName=\"shards\"\n" +
            "            shardMappingName=\"shardMap\"\n" +
            "            period=100\n" +
            "            cacheInvalidateTimeout=1" +
            "        }";

    private final String newConf = "redisShardManagerConfigurator {\n" +
            "            host=\"localhost\"\n" +
            "            shardSetName=\"shard\"\n" +
            "            shardMappingName=\"map\"\n" +
            "            period=1\n" +
            "            cacheInvalidateTimeout=1" +
            "        }";

    @Test
    public void testChangeConfig() throws IOException, InterruptedException {
        rewriteConfig(newConf);
        Thread.sleep(3000);
        Assert.assertEquals("shard", configurator.getShardSetName());
        Assert.assertEquals("map", configurator.getMapName());
        Assert.assertEquals("localhost", configurator.getHost());
        Assert.assertEquals(1, configurator.getPeriod());
        rewriteConfig(oldConf);
    }

    private void rewriteConfig(String conf) {
        File file = new File("/home/kkadoshnikov/sharding/src/test/resources/application.conf");
        try (PrintWriter out = new PrintWriter(file.getAbsoluteFile())) {
            out.print(conf);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
